package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		String messageId = request.getParameter("message_id");
		List<String> errorMessages = new ArrayList<String>();
		HttpSession session = request.getSession();

		if (!messageId.matches("^[0-9]+$") || StringUtils.isBlank(messageId)) {
			errorMessages.add("不正なパラメータです");
			session.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("./").forward(request, response);
			return;
		}

		// messageIdをつかって編集前の投稿を取得する
		int id = Integer.parseInt(messageId);
		UserMessage message = new MessageService().select(id);

		if (message == null) {
			errorMessages.add("不正なパラメータです");
			session.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("./").forward(request, response);
			return;
		}
		request.setAttribute("message", message);
		request.getRequestDispatcher("edit.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		// 編集済みの投稿を取得してupdate
		String text = request.getParameter("text");
		int messageId = Integer.parseInt(request.getParameter("message_id"));

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		if (!isValid(text, errorMessages)) {
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}
		Message message = new Message();
		message.setText(text);
		message.setId(messageId);
		new MessageService().update(message);
		response.sendRedirect("./");
	}

	private boolean isValid(String text, List<String> errorMessages) {

		if (StringUtils.isBlank(text)) {
			errorMessages.add("メッセージを入力してください");
		} else if (140 < text.length()) {
			errorMessages.add("140文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}

}
