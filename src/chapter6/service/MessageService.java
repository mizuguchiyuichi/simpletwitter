package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<UserMessage> select(Integer id, String startTimeDate, String endTimeDate) {
        final int LIMIT_NUM = 1000;
        final String DEFAULT_TIME_DATE = "2022-01-01 00:00:00";
        final String DEFAULT_START_TIME = " 00:00:00";
        final String DEFAULT_END_TIME = " 23:59:59";
        Connection connection = null;
        try {
            connection = getConnection();
            String startDate;
            String endDate;

            if(!StringUtils.isBlank(startTimeDate)) {
            	startDate = startTimeDate + DEFAULT_START_TIME;
            }else {
            	startDate = DEFAULT_TIME_DATE;
            }

            if(!StringUtils.isBlank(endTimeDate)) {
            	endDate = endTimeDate + DEFAULT_END_TIME;
            }else {
            	Date nowTimeDate = new Date();
            	SimpleDateFormat format = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
            	endDate = format.format(nowTimeDate);
            }

             List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, startDate, endDate);
            commit(connection);

            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void deleteMessage(String messageId) {

		Connection connection = null;

		try {
			connection = getConnection();
			int deleteMessageId = Integer.parseInt(messageId);

			new MessageDao().delete(connection, deleteMessageId);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}


	public UserMessage select(int id) {

		Connection connection = null;

		try {
			connection = getConnection();
			UserMessage message = new MessageDao().select(connection, id);
			commit(connection);

			return message;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update(Message message) {

		Connection connection = null;

		try {
			connection = getConnection();
			new MessageDao().update(connection,message);
			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			throw e;
		} finally {
			close(connection);
		}
	}
}