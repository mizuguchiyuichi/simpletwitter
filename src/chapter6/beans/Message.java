package chapter6.beans;

import java.io.Serializable;
import java.util.Date;

public class Message implements Serializable {

	private int id;
	private int userId;
	private String text;
	private String name;
	private String account;
	private Date createdDate;

	// getter
	public int getId() {
		return id;
	}

	public int getUserId() {
		return userId;
	}

	public String getName() {
		return name;
	}

	public String getAccount() {
		return account;
	}

	public String getText() {
		return text;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	// setter
	public void setId(int id) {
		this.id = id;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

}